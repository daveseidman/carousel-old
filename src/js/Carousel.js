// Carousel Module:
// Creates and positions dom elements in 2D space
//

let projects;
let windowWidth;
let half;
let quarter;


const createItem = (project, index) => {
  const item = {};
  item.el = document.createElement('div');
  item.el.style.backgroundColor = `rgb(
    ${Math.floor(Math.random() * 255)},
    ${Math.floor(Math.random() * 255)},
    ${Math.floor(Math.random() * 255)})`;

  item.el.className = 'item';
  item.el.setAttribute('data-id', index);
  if (project.home) item.el.classList.add('home');
  item.content = document.createElement('div');
  item.content.className = 'item-content';

  item.el.style.width = `${half}px`;
  item.el.style.left = `${(index * half) + quarter}px`;

  item.title = document.createElement('h1');
  item.title.className = 'item-content-title';
  item.title.innerText = project.name;

  item.headline = document.createElement('h2');
  item.headline.className = 'item-content-headline';
  item.headline.innerText = project.headline;

  item.tags = document.createElement('div');
  item.tags.className = 'item-content-tags';

  if (project.tags) {
    for (let i = 0, { length } = project.tags; i < length; i += 1) {
      const tag = document.createElement('span');
      tag.className = 'item-content-tags-tag';
      tag.innerText = project.tags[i];
      item.tags.appendChild(tag);
    }
  }

  if (project.links) {
    for (let i = 0, { length } = project.links; i < length; i += 1) {
      const link = document.createElement('a');
      const tag = document.createElement('span');
      tag.className = 'item-content-tags-tag';
      tag.innerText = project.links[i].text;
      link.setAttribute('target', '_blank');
      link.href = project.links[i].link;
      link.appendChild(tag);
      item.tags.appendChild(link);
    }
  }

  item.description = document.createElement('p');
  item.description.className = 'item-content-description';
  item.description.innerText = project.description;

  item.content.appendChild(item.title);
  item.content.appendChild(item.headline);
  item.content.appendChild(item.tags);
  item.content.appendChild(item.description);
  item.el.appendChild(item.content);

  return item;
};

const getID = (path) => {
  for (let i = 0, { length } = path; i < length - 5; i += 1) {
    if (path[i].getAttribute('data-id')) {
      return parseInt(path[i].getAttribute('data-id'), 10);
    }
  }
  return null;
};

const playPause = (video) => {
  if (video.paused) video.play();
  else video.pause();
};


export default class Carousel {
  constructor(_events, _projects, _windowWidth) {
    this.el = document.createElement('div');
    this.el.className = 'carousel';
    this.events = _events;
    this.itemOpen = false;
    projects = _projects;
    windowWidth = _windowWidth;
    half = windowWidth / 2;
    quarter = half / 2;
    const items = document.createElement('div');
    items.className = 'carousel-items';
    this.el.appendChild(items);
    this.projects = projects;


    projects.forEach((project, index) => {
      const item = createItem(project, index);
      project.open = false;
      project.el = item.el;
      project.content = item.content;
      project.video = item.video;
      items.appendChild(item.el);
    });

    this.carouselClicked = this.carouselClicked.bind(this);
    this.el.addEventListener('click', this.carouselClicked);
  }


  carouselClicked(e) {
    if (e.target.className.indexOf('item-content-video') >= 0) {
      playPause(e.target);
    } else {
      const id = getID(e.path);
      if (id !== null) this.events.emit('itemClicked', id);
    }
  }

  openItem(index) {
    const { content } = projects[index];
    projects[index].open = true;
    content.classList.add('item-content-open');
    this.el.classList.add('locked');
  }

  closeItem(index) {
    const { content } = projects[index];
    projects[index].open = false;
    content.classList.remove('item-content-open');
    this.el.classList.remove('locked');
  }

  update(position) { // eslint-disable-line
    for (let m = 0, { length } = projects; m < length; m += 1) {
      const { el } = projects[m];
      const offset = -position + el.offsetLeft;
      const range = (half - Math.abs(offset - half)) / half;
    }
  }

  resize(width) { // eslint-disable-line
    windowWidth = width;
    half = width / 2;
    quarter = half / 2;

    for (let i = 0, { length } = this.projects; i < length; i += 1) {
      const { el } = this.projects[i];
      el.style.width = `${half}px`;
      el.style.left = `${(i * half) + quarter}px`;
    }
  }
}
