import { roundTo, clamp, debug } from './utils';

const TWEEN = require('@tweenjs/tween.js');

const maxSpeed = 200;

let carouselWidth;
let windowWidth;
let half;
let quarter;
let eigth;

export default class Scroll {
  constructor(el, projects, _windowWidth) {
    this.el = el;
    this.projects = projects;
    this.total = projects.length;
    this.currentPosition = 0; // current position
    this.previousPosition = 0; // previous position
    this.currentSpeed = 0; // current velocity (positive or negative x-value)
    this.previousSpeed = 0; // last movement (from dragging)
    this.currentIndex = 0; // which project is current
    this.targetIndex = null; // where we're tweening towards
    this.previousIndex = 0; // previous current
    this.nextIndex = 1; // which project is next
    this.percent = 0; // what percent for morph
    this.movedLeft = false; // scroll direction
    this.lastTime = Date.now(); // last time carousel was scrolled by user (not tween)
    this.docking = null; // tween
    this.resizing = false;
    this.locked = false;

    this.currentPosition = el.scrollLeft;
    this.previousPosition = el.scrollLeft;

    this.listen = this.listen.bind(this);
    this.update = this.update.bind(this);
    this.scrolling = this.scrolling.bind(this);
    this.dock = this.dock.bind(this);
    this.resize = this.resize.bind(this);


    this.resize(_windowWidth);
    this.sendToFront(this.projects[this.projects.length - 1].el);

    if (process.env.NODE_ENV === 'development' && window.location.href.indexOf('debug') >= 0) document.body.appendChild(debug(this, ['projects', 'el']));
  }

  listen() {
    this.el.addEventListener('scroll', this.scrolling);
  }

  update() { //eslint-disable-line
    const scrollCentered = this.currentPosition + half;
    const scrollPercent = scrollCentered / carouselWidth;
    const currentRaw = ((this.total * scrollPercent) - 0.5) % this.total;
    const current = currentRaw > 0 ? Math.floor(currentRaw) : 0;
    const next = current + 1 < this.total ? current + 1 : 0;
    const percent = currentRaw > 0 ? currentRaw % 1 : 0;


    this.currentIndex = current;
    // if (this.previousIndex != this.currentIndex) console.log('current changed');
    this.previousIndex = this.currentIndex;

    this.currentSpeed *= 0.994;

    this.nextIndex = next;
    this.percent = percent;

    // page is being automatically scrolled to center of item
    if (this.docking) {
      TWEEN.update();
      // TODO: maybe move this into index.js
      this.el.scrollLeft = this.currentPosition;
      this.lastTime = null;
    }
    // user hasn't scrolled in more than half a second, complete the motion
    if (this.lastTime && Date.now() - this.lastTime > 500) this.scrollEnd();
  }

  // scrolled too far left, move everything right by 'width of carousel' pixels
  overscroll() {
    for (let m = 0, { length } = this.projects; m < length; m += 1) {
      const project = this.projects[m];
      const { el } = project;
      el.style.left = `${parseInt(el.style.left, 10) + carouselWidth}px`;
    }
    this.el.scrollLeft += carouselWidth;
    this.currentPosition = this.el.scrollLeft;
    this.previousPosition = this.currentPosition;
  }

  // smoothly stop the carousel at a specified target position
  dock(target, duration) {
    // this.dragging = false; // TODO should this be here or in Drag.js?

    if (target < quarter) {
      target += carouselWidth;
      this.overscroll();
    }

    this.docking = new TWEEN.Tween(this)
      .to({ currentPosition: target }, duration || 600)
      .onComplete(() => {
        setTimeout(() => {
          this.docking = null; // TODO this should be a flag to be picked up on the next update loop
          this.targetIndex = null;
          this.firstUpdate = false;
        }, 50);
      })
      .easing(TWEEN.Easing.Quadratic.Out)
      .start();
  }

  scrolling(e) {
    if (this.resizing) {
      this.el.scrollLeft = this.currentPosition;
      return e.preventDefault();
    }

    if (this.docking) this.lastTime = null;
    else this.lastTime = Date.now();

    this.currentPosition = this.el.scrollLeft;
    this.movedLeft = this.currentPosition > this.previousPosition;
    this.currentSpeed = clamp((this.currentPosition - this.previousPosition), -maxSpeed, maxSpeed);

    if (this.el.scrollLeft < quarter) return this.overscroll();

    // reposition elements that are too far left or right
    for (let m = 0, { length } = this.projects; m < length; m += 1) {
      const project = this.projects[m];
      const position = -this.el.scrollLeft + project.el.offsetLeft;
      if (this.movedLeft && position < -quarter) this.sendToBack(project.el);
      if (!this.movedLeft && position > windowWidth + quarter) this.sendToFront(project.el);
    }
    this.previousPosition = this.currentPosition;
    return true;
  }

  scrollEnd() {
    this.lastTime = null;
    let target = roundTo(this.currentPosition, half);

    const closeToTarget = Math.abs(target - this.currentPosition) > eigth;
    if (closeToTarget) { // dock to closest
      target += this.currentPosition < target ? -quarter : quarter;
    } else { // use momentum to go to next / prev
      target += this.movedLeft ? quarter : -quarter;
    }
    this.dock(target);
  }

  sendToBack(el) { // eslint-disable-line
    const position = parseInt(el.style.left, 10) + carouselWidth;
    el.style.left = `${position}px`;
  }

  // wrap an element to the beginning of the carousel
  sendToFront(el) { // eslint-disable-line
    const position = parseInt(el.style.left, 10) - carouselWidth;
    el.style.left = `${position}px`;
  }

  resize(width) { //eslint-disable-line
    windowWidth = width;
    half = windowWidth / 2;
    quarter = half / 2;
    eigth = quarter / 2;
    carouselWidth = this.projects.length * half;
  }
}
