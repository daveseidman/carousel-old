import { roundTo } from './utils';

let scroll;
let wrap;
let windowWidth;
let half;
let quarter;

const movementThreshold = 5;

export default class Drag {
  constructor(_scroll, _wrap) {
    this.locked = false;
    this.dragging = false;
    scroll = _scroll;
    wrap = _wrap;

    this.startDrag = this.startDrag.bind(this);
    this.drag = this.drag.bind(this);
    this.stopDrag = this.stopDrag.bind(this);

    wrap.addEventListener('mousedown', this.startDrag);
  }

  startDrag(e) {
    if (scroll.locked) return;
    this.startX = e.clientX;
    this.startY = e.clientY;
    scroll.dragging = true;
    if (scroll.docking) scroll.docking.stop();

    wrap.addEventListener('mousemove', this.drag);
    wrap.addEventListener('mouseup', this.stopDrag);
    wrap.addEventListener('mouseleave', this.stopDrag);
  }

  drag(e) {
    this.dragging = true;
    scroll.lastTime = null;
    scroll.previousSpeed = e.movementX;
    wrap.scrollLeft -= scroll.previousSpeed;
  }

  stopDrag(e) {
    setTimeout(() => { this.dragging = false; }, 10);
    wrap.removeEventListener('mousemove', this.drag);
    wrap.removeEventListener('mouseup', this.stopDrag);
    wrap.removeEventListener('mouseleave', this.stopDrag);

    // not enough movement
    if (Math.abs(e.clientX - this.startX) + Math.abs(e.clientY - this.startY) < movementThreshold)
      return;

    // multiply the last movement by -10 to determine where scroll would finish
    const scrollTarget = scroll.currentPosition + (scroll.previousSpeed * -10);

    // round it off so that it lands in the middle of a project
    let target = roundTo(scrollTarget, half) + (scroll.movedLeft ? -quarter : quarter);

    // prevent "bouncing back" if not dragged far enough
    if (Math.abs(scroll.currentPosition - target) < quarter) {
      target += scroll.movedLeft ? half : -half;
    }
    scroll.dock(target);
  }

  resize(_windowWidth) { // eslint-disable-line
    windowWidth = _windowWidth;
    half = windowWidth / 2;
    quarter = half / 2;
  }
}
