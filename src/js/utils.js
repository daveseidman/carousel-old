module.exports.debounce = (func, wait, immediate) => {
  let timeout;
  return () => {
    const context = this;
    const args = arguments;
    const later = () => {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    const callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

module.exports.roundTo = (num, mlt) => Math.round(num / mlt) * mlt;

module.exports.clamp = (num, min, max) => (num <= min ? min : num >= max ? max : num);

module.exports.debug = (object, exclude) => {
  const el = document.createElement('div');
  el.className = 'debug';

  const update = () => {
    const keys = Object.keys(object);
    let html = '';
    keys.forEach((key) => {
      const type = typeof object[key];
      if (type !== 'function' && exclude.indexOf(key) < 0) {
        let val = object[key];
        if (type === 'number') val = val.toFixed(2);
        html += `${key}: ${val}<br>`;
      }
    });
    el.innerHTML = html;

    requestAnimationFrame(update);
  };

  update();
  return el;
};
