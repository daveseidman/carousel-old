// TODO: resize doesn't go to correct item on complete
// TODO: load video's on demand
// TODO: add content
// TODO: tweak colors
// TODO: orientation based on offset from load, should be absolute
// TODO: add google analytics
// TODO: use translateX instead of left for carousel items
// TODO: scale up particles for first 10 frames
// TODO: safari load (spins too much)
// TODO: Particles should spin
// TODO: route on entry (if hash exists)

// TODO: scroll doesn't always finish on the correct index (needs tiny adjustment)
// TODO: add indicator dots to carousel

import EventEmitter from 'events';
import { debounce } from './js/utils';
import Scroll from './js/Scroll';
import Drag from './js/Drag';
import Carousel from './js/Carousel';
import './css/style.scss';

const events = new EventEmitter();
const projects = require('./data/projects.json');

// sizes
let windowHeight;
let windowWidth;
let half;
let quarter;

// elements
let wrap;
let scroll;
let drag;
let carousel;


const gotoNext = () => {
  let next;
  if (scroll.targetIndex) next = scroll.targetIndex + 1;
  else next = scroll.currentIndex + 1;
  next = next >= projects.length ? 0 : next;
  scroll.targetIndex = next;
  const project = projects[next];
  const { offsetLeft } = project.el;
  scroll.dock(Math.round(offsetLeft - half));
};

const gotoPrev = () => {
  let prev;
  if (scroll.targetIndex) prev = scroll.targetIndex - 1;
  else prev = scroll.currentIndex - 1;
  prev = prev < 0 ? projects.length - 1 : prev;
  scroll.targetIndex = prev;
  const project = projects[prev];
  const { offsetLeft } = project.el;
  scroll.dock(Math.round(offsetLeft - half));
};

const gotoItem = (index, force) => {
  if (index === scroll.currentIndex && !force)
    return;

  console.log('going to an item', index);
  // decide whether to go straight to item or wrap around
  // (ie, going from 2 to 4 should be a direct move whereas going from 8 to 1
  // is probably better to wrap around)
  const directDistance = Math.abs(index - scroll.currentIndex);
  const wrapDistance = Math.abs(projects.length - index) + scroll.currentIndex;
  if (directDistance < wrapDistance) {
    for (let i = 0; i < directDistance; i += 1) {
      if (index > scroll.currentIndex)
        gotoNext();
      else
        gotoPrev();
    }
  } else {
    for (let i = 0; i < wrapDistance; i += 1) {
      if (index > scroll.currentIndex)
        gotoPrev();
      else
        gotoNext();
    }
  }
};

const getIndexForPath = (path) => {
  for (let i = 0, { length } = projects; i < length; i += 1) {
    if (projects[i].url === path) return i;
  }
  return null;
};


const hashChange = () => {
  const path = window.location.hash.slice(1);
  const index = getIndexForPath(path);
  if (index) gotoItem(index);
};

const hotkeys = [
  48, 49, 50, 51, 52, 53, 54, 55, 56, 57,
  37, 39,
];

const keyDown = (e) => {
  if (hotkeys.indexOf(e.which) <= 0)
    return;

  e.preventDefault();

  if (scroll.locked)
    return;

  switch (e.which) {
    case 37: gotoPrev(); break;
    case 39: gotoNext(); break;
    default: gotoItem(hotkeys.indexOf(e.which));
  }
};

const itemClicked = (index) => {
  // user just finished dragging, do nothing
  if (drag.dragging)
    return;

  // item clicked is not in center
  if (index !== scroll.currentIndex) {
    // close item if it's open
    carousel.closeItem(scroll.currentIndex);
    scroll.locked = false;
    gotoItem(index);
  } else { // clicked on center item
    if (projects[index].open) {
      carousel.closeItem(index);
      scroll.locked = false;
    } else if (!projects[index].home) {
      carousel.openItem(index);
      scroll.locked = true;
    }
  }
};

const update = () => {
  scroll.update();

  requestAnimationFrame(update);
};

let startWidth;

const resizeStart = () => {
  scroll.resizing = true;
  startWidth = windowWidth;
};

const resize = () => {
  scroll.resizing = false;

  windowWidth = window.innerWidth;
  windowHeight = window.innerHeight;
  half = windowWidth / 2;
  quarter = half / 2;

  scroll.resize(windowWidth);
  drag.resize(windowWidth);
  carousel.resize(windowWidth);

  setTimeout(() => {
    if (windowWidth > startWidth) {
      gotoNext();
    } else {
      gotoNext();
      gotoPrev();
    }
  }, 200);
};


const listen = () => {
  window.addEventListener('resize', resizeStart);
  window.addEventListener('resize', debounce(resize, 500));
  window.addEventListener('hashchange', hashChange);
  window.addEventListener('keydown', keyDown);
  events.on('itemClicked', itemClicked);
  events.on('loadedOneModel', () => {
    console.log('loaded a model');
  });
};

const start = () => {
  const center = ((half * Math.floor(projects.length / 2)) - half) + quarter;
  carousel.el.scrollLeft = center;

  gotoItem(Math.floor(projects.length / 2), false);
  setTimeout(() => {
    scroll.listen();
  }, 500);
  update();
};

const init = () => {
  wrap = document.querySelector('.wrap');
  carousel = new Carousel(events, projects, windowWidth);
  scroll = new Scroll(carousel.el, projects, windowWidth);
  drag = new Drag(scroll, carousel.el, windowWidth);
  wrap.appendChild(carousel.el);

  resize();
  listen();
  start();
};


init();


// TODO: move to utils
if (!('path' in Event.prototype))
  Object.defineProperty(Event.prototype, 'path', {
    get() {
      const path = [];
      let currentElem = this.target;
      while (currentElem) {
        path.push(currentElem);
        currentElem = currentElem.parentElement;
      }
      if (path.indexOf(window) === -1 && path.indexOf(document) === -1)
        path.push(document);
      if (path.indexOf(window) === -1)
        path.push(window);
      return path;
    },
  });
