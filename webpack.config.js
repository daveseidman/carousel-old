const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: './src/index.js',
  devtool: 'inline-source-map',
  devServer: {
    host: '0.0.0.0',
    disableHostCheck: true,
    historyApiFallback: true,
    contentBase: './dist',
  },
  plugins: [
    new CleanWebpackPlugin(['dist']),
    new HtmlWebpackPlugin({
      inject: true,
      chunks: ['main'],
      title: 'Dave Seidman',
      template: 'src/index-template.html',
      filename: 'index.html',
    }),
    new CopyWebpackPlugin([{ from: './src/css/fonts', to: 'fonts ' }]),
  ],
  module: {
    rules: [{
      test: /\.scss$/,
      use: [
        { loader: 'style-loader' },
        { loader: 'css-loader' },
        { loader: 'sass-loader' },
      ],
    },
    {
      test: /\.(png|jpg|gif)$/,
      use: ['file-loader'],
    },
    {
      test: /\.(svg|ttf|woff|woff2)$/,
      use: ['url-loader'],
    }],
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
};
